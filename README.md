# PhD Thesis LuaLaTeX Template

This repository produces a
[PDF](https://gitlab.com/pothitos/diphdthesis-lua/-/jobs/artifacts/master/file/thesis.pdf?job=build)
according to the English
[template](https://www.di.uoa.gr/documents#tab-641-3) for
PhD thesis of the Department of Informatics and
Telecommunications of the National and Kapodistrian
University of Athens. A
[spine](https://gitlab.com/pothitos/diphdthesis-lua/-/jobs/artifacts/master/file/spine.pdf?job=build)
is produced too.

---

An [Open
Research](https://gist.github.com/pothitos/ec5f4f66ddd113aea6bac4094690d72e)
work
